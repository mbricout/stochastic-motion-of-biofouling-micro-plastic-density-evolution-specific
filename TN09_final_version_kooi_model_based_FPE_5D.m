%% Resume (Matlab file)

    % +------------------------------------------------------+
    % |        Solving FPE of biofouling Micro plastic:      |
    % |              Density evolution specific              |
    % |                  in stochastic motion                |
    % |              using Crank-Nicolson method             | 
    % |                                                      |
    % | Author: Maximilien Bricout                28/07/2022 | 
    % +------------------------------------------------------+
    
    
    % Specific: 
        % Solving P(x,y,rho,z,t)
        % rho [kg/m^3], z[m], x[km], y[km], t[day]
        % size of MP 1mm or 0.1mm
        % 3 differents density possible ( 840 PP;, 920 LDPE, 940 HDPE) 
        % Diffusion coef==0 for z,rho,x,y
        % Rho an z are acting on depth dimension, x on vertical and y on horizontale 
        % Editing a 5D visualization at the end

% Gitlab links: 
% email: maximilien.bricout@etu.utc.fr 

%% Start
    clear, clc, tic
    
    % custom parameters
     
    Micro_particle_size=0.1;              % 0.1 ||   1 mm
    Micro_particle_density=840;           % 840 || 920 || 940
    Simulation_time= 1;                  % 25 days max

    export_video=1;                       % 1=yes, 0=No
    Save_data=0;                          % 1=yes, 0=No                                    

%% Grid specific: z,x,y,t,rho 

%Space discretization
    N=32;           % default discretization step

    % Z   % spatial spacing %apatial grid sections
        % Max depth = f(size, density)
        if Micro_particle_size==0.1&&Micro_particle_density==840                          
                L=64.1;                
            elseif Micro_particle_size==1&&Micro_particle_density==840 
                L=75.1;                                                                           
            elseif Micro_particle_size==0.1&&Micro_particle_density==920
                L=55.51;                              
            elseif Micro_particle_size==1&&Micro_particle_density==920
                L=74.9;                                                                
            elseif Micro_particle_size==0.1&&Micro_particle_density==940
                L=51.47;                               
            elseif Micro_particle_size==1&&Micro_particle_density==940 
                L=74.6;                                
        end                  
        Nz=N; 
        z=-[0.5:Nz]'/Nz*L;                                                             
        dz=z(1)-z(2);  
    
    % X    % spatial spacing  %apatial grid sections
        Lx=800;
        Nx=N*2;
        x=[0.5:Nx]'/Nx*Lx;        
        dx=abs(x(1)-x(2)); 
    
    % Y   % spatial spacing  %apatial grid sections 
        Ly=800;
        Ny=N*2;
        y=[0.5:Ny]'/Ny*Ly;       
        dy=abs(y(1)-y(2));
    
    % Time discretization & precision
       tmax=Simulation_time;
       tsnap=32; 
       dt=1/1024;                                         

    % density discretization % Density spacing  
        dr=1 ;   

                     


%% Initial conditions
    P=zeros(Nz,Nx,Ny,1);                                            
    P(Nz/2,Nx/2,Ny/2)=1;
    t=0;
    sigz=0; 
    sigr=0;
    sigx=0;
    sigy=0;

%% Parameters: global result variable initialization

    Ps(:,:,:,1)=[P];                                      
    ts=[t];                                          

%% streamfunction parameters for x and y dimensions : Eddies
    
    amp=0.1;
    [k,l]=meshgrid([-3:0.5:3]);
    a=(k.*k+l.*l).^(-11/12);
    a((size(k,1)+1)/2,(size(k,1)+1)/2)=0;
    th=2*pi*rand(size(a,1),1);
    [xg,yg]=meshgrid(x,y);
    dth=sqrt(dt);
    c=((xg-Nx/2*dx).^2+(yg-Nx/2*dx).^2)<=1;
    kappa=1e-2;
    [xp,yp]=meshgrid([0:Nx]*dx);


%% Solving FPE
    
    while t<tmax
    
    % streamfunction parameters set up
              psiv2=psi(xp,yp,k,l,a,th,amp);
              [dc,fx,fy]=upw(psiv2,c,dx,0,0);
              dcdt=dc+diffuse(c,kappa,dx,0,0);
              c =c+ dcdt*dt;
              t=t+dt;
              th = th+ dth*(rand(size(a))-0.5);
      
    % At t    %Flux parameters
              w= velzk(t,Micro_particle_size,Micro_particle_density);                                                                            
              rhop= dens(t,Micro_particle_size,Micro_particle_density);
              u=velx(psiv2,dy);
              v=vely(psiv2,dx);
              %Flux calculus
              [Fw,Fsw,Fr,Fsr,Fu,Fsu,Fv,Fsv]=flux(P,w,sigz,rhop,sigr,u,sigx,v,sigy,Nz,Nx,Ny);                                                                    
              % First part of FPE resolution 
              dP = dt*( (Fw(2:end,1:end,1:end)-Fw(1:end-1,1:end,1:end))/(dz)   +(Fsw(2:end,1:end,1:end)+Fsw(1:end-1,1:end,1:end))/(dz^2)    ...
                   +(Fr(2:end,1:end,1:end)-Fr(1:end-1,1:end,1:end))/(dr)   +(Fsr(2:end,1:end,1:end)+Fsr(1:end-1,1:end,1:end))/(dr^2)            ...
                   +(Fu(1:end,2:end,1:end)-Fu(1:end,1:end-1,1:end))/(dr)   +(Fsu(1:end,2:end,1:end)+Fsu(1:end,1:end-1,1:end))/(dr^2)                ...
                   +(Fv(1:end,1:end,2:end)-Fv(1:end,1:end,1:end-1))/(dr)   +(Fsv(1:end,1:end,2:end)+Fsv(1:end,1:end,1:end-1))/(dr^2) );                                
            
    
    % At t+dt
              t=t+dt;
              %Flux parameters
              w=velzk(t,Micro_particle_size,Micro_particle_density);                                                                          
              rhop= dens(t,Micro_particle_size,Micro_particle_density);
              u=velx(psiv2,dy);
              v=vely(psiv2,dx);
              %Flux calculus
              [Fw,Fsw,Fr,Fsr,Fu,Fsu,Fv,Fsv]=flux(P,w,sigz,rhop,sigr,u,sigx,v,sigy,Nz,Nx,Ny);                                                                     
              % Second part of FPE resolution 
              dP1 = dt*( (Fw(2:end,1:end,1:end)-Fw(1:end-1,1:end,1:end))/(dz)   +(Fsw(2:end,1:end,1:end)+Fsw(1:end-1,1:end,1:end))/(dz^2)    ...
                   +(Fr(2:end,1:end,1:end)-Fr(1:end-1,1:end,1:end))/(dr)   +(Fsr(2:end,1:end,1:end)+Fsr(1:end-1,1:end,1:end))/(dr^2)            ...
                   +(Fu(1:end,2:end,1:end)-Fu(1:end,1:end-1,1:end))/(dr)   +(Fsu(1:end,2:end,1:end)+Fsu(1:end,1:end-1,1:end))/(dr^2)           ...
                   +(Fv(1:end,1:end,2:end)-Fv(1:end,1:end,1:end-1))/(dr)   +(Fsv(1:end,1:end,2:end)+Fsv(1:end,1:end,1:end-1))/(dr^2) ); 
    
   % Solving FPE 
              P=(P*(1-sigz*dt/dz^2-sigr*dt/dr^2-sigx*dt/dx^2-sigy*dt/dy^2)+0.5*(dP+dP1))/(1+sigz*dt/dz^2+sigr*dt/dr^2+sigx*dt/dx^2+sigy*dt/dy^2);                                  
    
    
   % live plot FPE: depth=f(probability)          
              if rem(t,tsnap*dt)==0                            
                ts=[ts,t];                                              
                Ps(:,:,:,end+1)=[P];  
                plot(P(:,Nx/2,Ny/2),z);
                title(['Data = \it{P} \rm(Z, t) @ t = ' num2str(t)]) 
                drawnow();
              end
    end


%% Plot 5D, 2D & video export
   % Set up for video exportation
            if Save_data==1; save('Datasaved/Datapar.mat','x','y','z','ts'); end
   % Set up for visualization
            x=x(1:end,1)';
            y=y(1:end,1)';
            z=z(1:end,1)';
            t=ts(1,1:end);
            [X, Y, Z] = meshgrid(x, z, y);
    
    % form the data matrix - it is the fifth dimension
            Data=Ps(1:end,1:end,1:end,1:end);
            Data2Dxy=squeeze(sum(Data,1));
            Data2Dzy=squeeze(sum(Data,2));
            Data2Dzx=squeeze(sum(Data,3));
            Data2Dxy(Data2Dxy<=0.00001)=NaN;    %enable clear comprehension of graphics
            Data(Data<=0.00001)=NaN;            %enable clear comprehension of graphics
            
    % organize the visualization
            figure(1) 
            hScatter = scatter3(X(:), Z(:), Y(:), 'filled');
            colormap cool
            grid3 on
            set(gca, 'FontName', 'Times New Roman', 'FontSize', 14)
            xlabel('Horizontal (km)')
            ylabel('Vertical (km)')
            zlabel('Depth (m)')
            c = colorbar;
            text(48,-1000,-32,'Probability')

    % saving video 
            if export_video==1
                    if isunix % for linux
                    pathVideoAVI = '~/someVideo2.avi'; % filename, used later to generate mp4
                    elseif ispc % fow windows
                    pathVideoAVI = 'Repport/someVideo2.avi'; % filename, used later to generate mp4
                    end
                    writerObj = VideoWriter(pathVideoAVI,'Uncompressed AVI');
                    writerObj.FrameRate=10;
                    open(writerObj);
                    if isunix % for linux
                    pathVideoAVI = '~/someVideo2.avi'; % filename, used later to generate mp4
                    elseif ispc % fow windows
                    pathVideoAVI = 'Repport/someVideo3.avi'; % filename, used later to generate mp4
                    end
                    writerObj2 = VideoWriter(pathVideoAVI,'Uncompressed AVI');
                    writerObj2.FrameRate=10;
                    open(writerObj2);
            end


            
    % cycle through the fourth dimension independent variable
            for k = 1:size(t,2) 
                
                figure(1)
                C = reshape(Data(:, :, :,k), length(x)*length(y)*length(z), 1);         % visualize the fifth dimension via the marker color
                set(hScatter, 'CData', C, 'SizeData', 10)                               % update the plot
                title([' \rm\it Figure 8: Probability distribution represented for a PP particle of 0.1mm @ t = ' num2str(t(k)),newline])
                drawnow
                pause(0.1)


                if export_video==1
                                if k~=1
                                frame = getframe(gcf);
                                writeVideo(writerObj,frame);
                                end
                end
  
            
    % 2Dplot : x/y
                figure(2)
                    h=pcolor(x,y,Data2Dxy(:,:,k));
                    set(h, 'EdgeColor', 'none');
                    set(gca,'color',[0.95 0.95 0.95])
                    title(['\rm\it Figure 7: Probability distribution represented for a PP particle of 0.1mm @ t = ' num2str(t(k)),newline])
                    xlabel('Horizontal (km)')
                    ylabel('Vertical (km)')
                    colormap cool
                    c = colorbar;
                    text(795,-10,'Probability')
                    drawnow
    % 2nd video
                if export_video==1
                if k~=1
                frame = getframe(gcf);
                writeVideo(writerObj2,frame);
                end
                end
            
            end
    % video converter
                if export_video==1
                % Close the movie file
                close(writerObj); 
                % convert AVI to MP4
                pathVideoMP4 = regexprep(pathVideoAVI,'\.avi','.mp4'); % generate mp4 filename
                if isunix % for linux
                    [~,~] = system(sprintf('ffmpeg -i %s -y -an -c:v libx264 -crf 0 -preset slow %s',pathVideoAVI,pathVideoMP4)); % for this to work, you should have installed ffmpeg and have it available on PATH
                elseif ispc % for windows
                    [~,~] = system(sprintf('ffmpeg.exe -i %s -y -an -c:v libx264 -crf 0 -preset slow %s',pathVideoAVI,pathVideoMP4)); % for this to work, you should have installed ffmpeg and have it available on PATH
                end
                % Close the movie file
                close(writerObj2); 
                % convert AVI to MP4
                pathVideoMP4 = regexprep(pathVideoAVI,'\.avi','.mp4'); % generate mp4 filename
                if isunix % for linux
                    [~,~] = system(sprintf('ffmpeg -i %s -y -an -c:v libx264 -crf 0 -preset slow %s',pathVideoAVI,pathVideoMP4)); % for this to work, you should have installed ffmpeg and have it available on PATH
                elseif ispc % for windows
                    [~,~] = system(sprintf('ffmpeg.exe -i %s -y -an -c:v libx264 -crf 0 -preset slow %s',pathVideoAVI,pathVideoMP4)); % for this to work, you should have installed ffmpeg and have it available on PATH
                end
                end 

toc                                                                       % End timer 
%% Data & functions extract from article

    % Flow  Function method 
            function [Fw,Fsw,Fr,Fsr,Fu,Fsu,Fv,Fsv]=...
                                        flux(P,w,sigwk,rhop,sigr,u,sigx,v,sigy,Nz,Nx,Ny)    
       % Flux
            pa=P(1:end-1,1:end,1:end)+P(2:end,1:end,1:end);
            pd=P(1:end-1,1:end,1:end)-P(2:end,1:end,1:end);
            pa2=P(1:end,1:end-1,1:end)+P(1:end,2:end,1:end);
            pd2=P(1:end,1:end-1,1:end)-P(1:end,2:end,1:end);
            pa3=P(1:end,1:end,1:end-1)+P(1:end,1:end,2:end);
            pd3=P(1:end,1:end,1:end-1)-P(1:end,1:end,2:end);
      % Z velocity + diffusion
            Fw=[0.5*w.*pa-0.5*abs(w).*pd];
            Fw= [zeros(1,Nx,Ny); Fw  ; zeros(1,Nx,Ny) ];
            Fsw=[sigwk.*pd];
            Fsw= [zeros(1,Nx,Ny); Fsw  ; zeros(1,Nx,Ny) ];
      % Rho velocity + diffusion
            Fr=[0.5*rhop.*pa-0.5*abs(rhop).*pd];
            Fr= [zeros(1,Nx,Ny); Fr  ; zeros(1,Nx,Ny) ];
            Fsr=[sigr.*pd];
            Fsr= [zeros(1,Nx,Ny); Fsr  ; zeros(1,Nx,Ny) ];
      % X velocity + diffusion
            u=u(1:Nz,1:Nx-1);               %% over simplified
            Fu=[1*(0.5*u.*pa2-0.5*abs(u).*pd2)];
            Fu= [zeros(Nz,1,Ny) Fu zeros(Nz,1,Ny) ];
            Fsu=[sigx.*pd2];
            Fsu= [zeros(Nz,1,Ny) Fsu zeros(Nz,1,Ny)  ];
      %Y velocity + diffusion
            for i=1:size(v,1)
            vv(1,:,i)=v(i,:,1);
            end
            for i=1:size(v,1)
            vvv(:,:,i)=repmat(vv(1,:,i),Nz+1,1);
            end
            v=vvv;
            v=v(1:Nz,1:Nx,1:Ny-1);
            Fv=[1*(0.5*v.*pa3-0.5*abs(v).*pd3)];
            RR=zeros(Nz,Nx,1);
            Fv= cat(3,RR,Fv,RR);
            Fsv=[sigy.*pd3];
            RR=zeros(Nz,Nx,1);
            Fsv= cat(3,RR,Fsv,RR);
            end 
    
    % Calcul Velocity from Kooi model
            function Wk=velzk(t,size,density)
                
                if size==0.1&&density==840
                    t=mod(t,6);
                    Wk=(4073*cos((517*t)/100))/1000 + (958*cos((517*t)/125))/25 - (5279*cos((517*t)/250))/100 + (527*cos((517*t)/500))/100 + (4577*cos((1034*t)/125))/500 + (2227*cos((1551*t)/250))/100 - (4377*cos((1551*t)/500))/500 + (1881*cos((3619*t)/500))/500 + (6749*sin((517*t)/100))/1000 - (4591*sin((517*t)/125))/1000 - (3919*sin((517*t)/250))/100 + (2351*sin((517*t)/500))/1000 + (4187*sin((1034*t)/125))/1000 - (399*sin((1551*t)/250))/20 + (13*sin((1551*t)/500))/5 - (1963*sin((3619*t)/500))/250 + 2739/1000;
                end
                
                if size==1&&density==840
                       t=mod(t,1);
                    if t<0.2&&t>0.63
                        Wk=0;
                    else 
                        Wk= (1547*cos((1329*t)/20))/25 - (541*cos((1329*t)/25))/5 - (8883*cos((1329*t)/50))/10 + (1016*cos((1329*t)/100))/5 - (2153*cos((2658*t)/25))/50 - (215*cos((3987*t)/50))/2 - (918*cos((3987*t)/100))/5 + (1929*cos((9303*t)/100))/20 + (1006*sin((1329*t)/20))/5 + (8577*sin((1329*t)/25))/100 - 163*sin((1329*t)/50) + (249*sin((1329*t)/100))/20 + (6971*sin((2658*t)/25))/100 + (21*sin((3987*t)/50))/10 + (6921*sin((3987*t)/100))/1000 - (233*sin((9303*t)/100))/4 + 1367/100;
                    end
                
                end
                
                if size==10&&density==840
                        
                            t=mod(t,1); 
                        if t<0.01
                            Wk=0;
                        elseif t>0.01&&t<0.1
                            Wk=235500*sin(263*t + 53/40) + 4414*sin((1043*t)/2 - 2559/1000) + 4172*sin((4107*t)/10 + 877/250) + 233400*sin((2627*t)/10 - 1793/1000) + 2719*sin((5847*t)/10 - 2159/1000) + (4348*sin((8013*t)/10 + 3581/1000))/5 + 4071*sin((1651*t)/5 - 2171/50000);
                        elseif t>0.1&&t<0.25
                            Wk=(3208*sin((3527*t)/5 - 2701/1000))/5 + (2179*sin((6533*t)/10 - 1001/500))/10 + 1631*sin((9057*t)/10 + 157/100) + 56920*sin((7357*t)/10 + 1747/500) + 15080*sin((9651*t)/10 + 1681/500) + 57370*sin((7351*t)/10 - 5799/1000) + 14470*sin((9667*t)/10 + 6301/1000) + 1444*sin((4188*t)/5 + 4179/10000);
                        elseif t>0.25&&t<0.302
                        Wk=4517*cos((199*t)/2) - 565*cos(199*t) - 4882*cos(398*t) + (1985*cos(597*t))/2 - 4165*cos((597*t)/2) + 3409*cos(796*t) - 2301*cos((995*t)/2) + (1422*cos((1393*t)/2))/5 - 5946*sin(199*t) - 5138*sin((199*t)/2) + (5477*sin(398*t))/10 + 2624*sin(597*t) - 3484*sin((597*t)/2) + 2015*sin(796*t) + 3482*sin((995*t)/2) + (9889*sin((1393*t)/2))/10 + 3628;
                        else
                            Wk=0;
                        end 
                
                
                
                end
                
                if size==0.1&&density==920
                
                    t=mod(t,2.96);
                    Wk=(942*sin((1749*t)/200 + 317/250))/25 + (183*sin((2069*t)/500 + 521/250))/10 + (969*sin((2149*t)/1000 - 1239/500))/20 + (2163*sin((6241*t)/1000 + 2489/1000))/100 + (258*sin((1581*t)/100 - 4827/5000))/125 + (967*sin((343*t)/25 - 2843/10000))/500 + (1819*sin((8751*t)/1000 + 4191/1000))/50 + (64*sin((67*t)/5 - 8071/10000))/625;
                 
                end
                
                if size==1&&density==920
                    t=mod(t,1);
                    if t<0.65||t>0.93
                    Wk=0;
                    else
                    Wk = (4371*cos((411*t)/5))/500 - (1676*cos((411*t)/25))/5 - (1023*cos((822*t)/25))/10 + (1264*cos((1233*t)/25))/5 - (2392*cos((1644*t)/25))/25 - (776*cos((2466*t)/25))/25 + (361*cos((2877*t)/25))/25 + (1039*cos((3288*t)/25))/50 - (6563*sin((411*t)/5))/100 + (3062*sin((411*t)/25))/5 + (2877*sin((822*t)/25))/50 + (1821*sin((1233*t)/25))/100 - (83*sin((1644*t)/25))/2 + (9317*sin((2466*t)/25))/100 - (5371*sin((2877*t)/25))/100 + (926*sin((3288*t)/25))/25 + 571/50;
                    end 
                
                end
                
                if size==10&&density==920
                        t=mod(t,1);
                        if t<0.7138
                            Wk=0;
                        elseif t>0.7138&&t<=0.8
                           % w=4048*sin(487*t + 1451/100) + (2233*sin((4009*t)/10 - 371/20))/10 + 2570*sin((3209*t)/5 - 1129/125) + 3890*sin((4961*t)/5 - 216/5) + 3363*sin((4741*t)/10 - 1389/50) + 2043*sin((5659*t)/10 - 429/500) + 1734*sin((6587*t)/10 + 2431/100) + 4008*sin((1991*t)/2 + 7399/1000)
                            Wk=3137*cos((2999*t)/10) + (7993*cos((2999*t)/25))/100 + (736*cos((2999*t)/50))/125 + 2762*cos((5998*t)/25) + (4862*cos((8997*t)/25))/5 + 925*cos((8997*t)/50) + (7931*cos((11996*t)/25))/10 - 2841*cos((20993*t)/50) - 1858*sin((2999*t)/10) + (2006*sin((2999*t)/25))/5 + (1364*sin((2999*t)/50))/5 - (3517*sin((5998*t)/25))/10 + (3823*sin((8997*t)/25))/5 + (7613*sin((8997*t)/50))/10 + 1651*sin((11996*t)/25) + (1817*sin((20993*t)/50))/10 + 1172/25;
                         
                        elseif t>0.8&&t<=0.9
                            Wk=4048*sin(487*t + 1451/100) + (2233*sin((4009*t)/10 - 371/20))/10 + 2570*sin((3209*t)/5 - 1129/125) + 3890*sin((4961*t)/5 - 216/5) + 3363*sin((4741*t)/10 - 1389/50) + 2043*sin((5659*t)/10 - 429/500) + 1734*sin((6587*t)/10 + 2431/100) + 4008*sin((1991*t)/2 + 7399/1000);
                        elseif t>0.9&&t<=0.96
                            Wk=2301*sin(670*t + 296/5) + (1841*sin(1478*t - 18))/25 + (4206*sin(1353*t + 321/125))/5 + 84*sin((2207*t)/5 - 2371/100) + 2694*sin((5523*t)/10 - 2837/100) + (1361*sin((9511*t)/10 - 1177/125))/100 + 2683*sin((5539*t)/10 + 6767/100) + 322*sin((7309*t)/10 + 9707/100);
                        else
                            Wk=3357*sin(108*t - 1507/50) + (2788*sin(1240*t + 461/125))/5 + 3578*sin((2947*t)/10 + 332/25) + (2097*sin(1399*t + 821/1250))/10 + 4961*sin((405*t)/2 - 4413/100) + (717*sin((4659*t)/5 - 1979/2500))/2 + 2082*sin((3103*t)/5 - 1659/5000) + 2018*sin((2282*t)/5 + 9379/1000);
                        end
                end
                
                if size==0.1&&density==940
                    t=mod(t,6.04);
                    Wk=(1229*sin((79*t)/5 - 309/200))/1000 + (3703*sin((923*t)/250 - 679/250))/100 + (457*sin((1567*t)/250 + 667/500))/25 + (2053*sin((304*t)/25 - 3049/500))/1000 + (1769*sin((1032*t)/125 - 2419/1000))/250 + (4749*sin((713*t)/500 - 4249/1000))/500 + (3643*sin((272*t)/125 - 4411/5000))/100 + (2329*sin((387*t)/100 - 1951/25000))/50;
                end
                
                if size==1&&density==940
                        t=mod(t,1);
                        if t<0.26&&t>0.59
                            Wk=0;
                        else
                            Wk=1386*sin((1427*t)/10 - 571/200) + 1345*sin(143*t + 3239/500) + (1168*sin((1787*t)/20 + 1943/1000))/25 + (5083*sin((1289*t)/10 - 2147/2500))/100 + (1641*sin((4639*t)/100 + 849/500))/10 + (1171*sin((6439*t)/100 - 741/250))/10 + 4365*sin((3029*t)/10000 + 1503/500) + (1637*sin((147*t)/10 - 7557/100000))/2;
                        end 
                end
                
                if size==10&&density==940
                        t=mod(t,1);
                        if t<0.21||t>0.5
                            Wk=0;
                        elseif t>0.21&&t<=0.3
                            Wk=132900*sin((666*t)/5 + 1491/100) + 2373*sin((2239*t)/10 + 773/125) + 3329*sin((1427*t)/5 + 2351/500) + 1106*sin(457*t - 5399/1000) + 2318*sin((745*t)/2 + 1499/5000) + (3559*sin((5679*t)/10 - 1401/2000))/5 + (6097*sin((3849*t)/5 - 4479/1000))/10 + 132200*sin((1329*t)/10 - 7201/10000);
                        elseif t>0.3&&t<=0.45
                            Wk=3883*sin((2234*t)/5 - 1139/100) + (6733*sin((1347*t)/2 - 1917/1000))/100 + 3475*sin(633*t - 3103/1000) + 4658*sin((2268*t)/5 + 1627/1000) + (1803*sin(1209*t + 4817/1000))/5 + 4130*sin((6269*t)/10 - 1669/100) + 1857*sin((5633*t)/10 - 2389/500) + 1496*sin((4957*t)/10 + 1691/2500);
                        elseif t>0.45&&t<=0.5
                            Wk=(1489*cos((1341*t)/5))/10 - (2104*cos((1341*t)/2))/5 - (597*cos((1341*t)/10))/5 - 1123*cos((2682*t)/5) + (6571*cos((4023*t)/5))/100 - (2621*cos((4023*t)/10))/10 - (2772*cos((5364*t)/5))/5 - (3423*cos((9387*t)/10))/100 - (2114*sin((1341*t)/2))/25 - (1721*sin((1341*t)/5))/10 + (1461*sin((1341*t)/10))/10 - 2283*sin((2682*t)/5) - (6583*sin((4023*t)/5))/100 + (4981*sin((4023*t)/10))/10 - (4497*sin((5364*t)/5))/10 + (1508*sin((9387*t)/10))/5 + 2107/10;
                        end 
                end
    
    
    
    end
    
    %Calcul diff(rho) From Kooi model
            function rhop=dens(t,size,density)
                
                if size==0.1&&density==840
                    t=mod(t,6);
                    rhop=(1573887*cos((1043*t)/125))/62500 + (2068269*cos((1043*t)/200))/1000000 - (2381169*cos((1043*t)/250))/125000 + (149149*cos((1043*t)/500))/3125 - (1265159*cos((1043*t)/1000))/250000 + (8751813*cos((3129*t)/500))/500000 - (5967003*cos((3129*t)/1000))/2000000 + (13601763*cos((7301*t)/1000))/2000000 + (4704973*sin((1043*t)/125))/125000 - (5301569*sin((1043*t)/200))/2000000 + (126203*sin((1043*t)/250))/3125 - (2276869*sin((1043*t)/500))/50000 - (1311051*sin((1043*t)/1000))/1000000 + (4415019*sin((3129*t)/500))/50000 - (4189731*sin((3129*t)/1000))/1000000 - (3570189*sin((7301*t)/1000))/10000000;
                end
                
                if size==1&&density==840
                       t=mod(t,1);
                       rhop=(21570519*cos((3219*t)/100))/1000000 - (6994887*cos((3219*t)/125))/1250000 + (280053*cos((3219*t)/250))/15625 + (1232877*cos((3219*t)/500))/31250 - (1503273*cos((6438*t)/125))/31250 - (16696953*cos((9657*t)/250))/1250000 - (4316679*cos((9657*t)/500))/125000 + (10072251*cos((22533*t)/500))/5000000 + (782217*sin((3219*t)/100))/20000 - (1619157*sin((3219*t)/125))/62500 + (12074469*sin((3219*t)/250))/250000 + (3705069*sin((3219*t)/500))/50000 - (14495157*sin((6438*t)/125))/3125000 + (21235743*sin((9657*t)/250))/1250000 + (5301693*sin((9657*t)/500))/100000 + (44818137*sin((22533*t)/500))/2500000;
                end
                
                if size==10&&density==840        
                       t=mod(t,1); 
                       rhop =(293546*cos((484*t)/125 + 1413/500))/125 + (4004277*cos((3013*t)/500 + 201/40))/5000 + (436887*cos((1471*t)/100 + 2133/500))/25000 + (1326699*cos((891*t)/500 + 7027/10000))/500;
                end
                
                if size==0.1&&density==920
                    t=mod(t,2.96);
                    rhop=(291279*cos((453*t)/125))/25 - (998714*cos((151*t)/125))/125 - (703056*cos((302*t)/125))/125 - (181351*cos((151*t)/25))/50 + (1151828*cos((604*t)/125))/125 - (477009*cos((906*t)/125))/125 + (5772277*cos((1057*t)/125))/125000 + (821742*cos((1208*t)/125))/3125 - (173046*sin((151*t)/25))/25 - (220007*sin((151*t)/125))/125 + (306832*sin((302*t)/125))/25 + (221517*sin((453*t)/125))/25 - (195092*sin((604*t)/125))/25 + (125481*sin((906*t)/125))/125 + (888937*sin((1057*t)/125))/625 + (923969*sin((1208*t)/125))/15625;
                end
                
                if size==1&&density==920
                    t=mod(t,1);
                    rhop=(9354753*cos((6249*t)/125))/625000 + (22227693*cos((6249*t)/200))/10000000 + (6955137*cos((6249*t)/250))/625000 - (1680981*cos((6249*t)/500))/125000 + (21140367*cos((6249*t)/1000))/500000 - (22327677*cos((18747*t)/500))/2500000 - (49848273*cos((18747*t)/1000))/5000000 + (11416923*cos((43743*t)/1000))/2500000 + (6167763*sin((6249*t)/125))/625000 + (1580997*sin((6249*t)/200))/80000 + (543663*sin((6249*t)/250))/100000 + (19628109*sin((6249*t)/500))/500000 + (3380709*sin((6249*t)/1000))/62500 + (5642847*sin((18747*t)/500))/1000000 + (206217*sin((18747*t)/1000))/125000 + (271577641349853201153*sin((43743*t)/1000))/1152921504606846976000;
                end
                
                if size==10&&density==920
                       t=mod(t,1);
                       rhop=(3652123*cos((3173*t)/250))/1250000 - (6304751*cos((3173*t)/125))/625000 - (6088987*cos((3173*t)/100))/10000000 - (14941657*cos((3173*t)/500))/500000 + (2160813*cos((6346*t)/125))/312500 + (23826057*cos((9519*t)/250))/2500000 + (15982401*cos((9519*t)/500))/1250000 + (19479047*cos((22211*t)/500))/10000000 - (4889593*sin((3173*t)/100))/1000000 - (16966031*sin((3173*t)/125))/12500000 + (7326457*sin((3173*t)/250))/250000 - (12733249*sin((3173*t)/500))/250000 + (1294584*sin((6346*t)/125))/78125 + (38466279*sin((9519*t)/250))/12500000 - (13412271*sin((9519*t)/500))/500000 - (10461381*sin((22211*t)/500))/500000;
                end
                
                if size==0.1&&density==940
                    t=mod(t,6.04);
                    rhop=(66462*cos((209*t)/25))/3125 - (922317*cos((209*t)/40))/400000 - (704957*cos((209*t)/50))/50000 - (63327*cos((209*t)/100))/2000 + (232617*cos((209*t)/200))/100000 - (88407*cos((627*t)/100))/6250 - (89661*cos((627*t)/200))/50000 - (4209051*cos((1463*t)/200))/1000000 - (72523*sin((209*t)/25))/6250 - (252263*sin((209*t)/40))/200000 - (1148037*sin((209*t)/50))/50000 - (202939*sin((209*t)/100))/10000 - (257697*sin((209*t)/200))/200000 + (5010357*sin((627*t)/100))/100000 - (765567*sin((627*t)/200))/200000 + (19019*sin((1463*t)/200))/25000;
                end
                
                if size==1&&density==940
                        t=mod(t,1);
                        rhop=(509877*cos((313*t)/10))/250000 - (736489*cos((313*t)/25))/62500 + (885477*cos((313*t)/50))/25000 + (56653*cos((626*t)/25))/6250 - (979377*cos((939*t)/25))/125000 - (130521*cos((939*t)/50))/15625 + (387181*cos((1252*t)/25))/31250 + (3994193*cos((2191*t)/50))/1000000 + (837901*sin((313*t)/10))/50000 + (827259*sin((313*t)/25))/25000 + (2268311*sin((313*t)/50))/50000 + (116749*sin((626*t)/25))/25000 + (273249*sin((939*t)/25))/62500 + (4786083*sin((939*t)/50))/5000000 + (491723*sin((1252*t)/25))/62500 + (225673*sin((2191*t)/50))/200000; 
                end
                
                if size==10&&density==940
                        t=mod(t,1);
                        rhop= (12262257*cos((3171*t)/250))/2500000 - (434427*cos((3171*t)/125))/50000 - (885827984275543869*cos((3171*t)/100))/11529215046068469760 + (3136119*cos((3171*t)/500))/125000 + (5990019*cos((6342*t)/125))/625000 + (4937247*cos((9513*t)/250))/625000 - (46299771*cos((9513*t)/500))/5000000 + (41441799*cos((22197*t)/500))/12500000 + (3383457*sin((3171*t)/100))/1000000 + (17760771*sin((3171*t)/125))/12500000 + (6148569*sin((3171*t)/250))/250000 + (20652723*sin((3171*t)/500))/500000 + (7486731*sin((6342*t)/125))/625000 + (38518137*sin((9513*t)/250))/25000000 + (2349711*sin((9513*t)/500))/100000 + (41974527*sin((22197*t)/500))/2500000; 
                end
    
    
    
    end
    
    % Velocity U
            function U=velx(psiv2,dy) 
            U=-psiv2*2400/dy;
            end 
    
    % Velocity V
            function V=vely(psiv2,dx)
            V=psiv2*2400/dx;
            end
    
    % streamfunction 
            function dc=diffuse(c,k,dx,Lx,Ly)
              n=size(c,1);np1=n+1;np2=n+2;
              ce=[c(:,n)-Lx,c,c(:,1)+Lx];
              ce=[ce(n,:)-Ly;ce;ce(1,:)+Ly];
              dc=k/dx^2*(-4*c+ce(2:np1,1:n)+ce(2:np1,3:np2)+ce(1:n,2:np1)+ce(3:np2,2:np1));
            end
    
    % streamfunction 
            function [dc,fx,fy]=upw(p,c,dx,Lx,Ly)
              n=size(c,1);
              u=-1/dx*diff(p);
              cl=[c(:,n)-Lx,c];
              cr=[c,c(:,1)+Lx];
              ca=cr+cl;
              cd=cr-cl;
              fx=0.5*(u.*ca-abs(u).*cd);
              v=1/dx*diff(p,1,2);
              cl=[c(n,:)-Ly;c];
              cr=[c;c(1,:)+Ly];
              ca=cl+cr;
              cd=cr-cl;
              fy = 0.5*(v.*ca-abs(v).*cd);
              dc=-1/dx*(diff(fx,1,2)+diff(fy));
            end
    
    % streamfunction  
            function p=psi(xp,yp,k,l,a,th,amp)
            p=0;
            for j=1:size(a,1)
              p=p+amp*a(j)*cos(k(j).*xp+l(j).*yp+th(j));
            end
            end
    
    
    
    
    
%Ps: suppress warning message
    %#ok<*AGROW>
    %#ok<*NBRAK> 
    %#ok<*SAGROW> 






