%% Resume (Matlab file)

    % +--------------------------------------------------------------------+
    % |                     Solving FPE with 5 variables                   |
    % | [Time (t), Vertical (x), Horizontal (y), depth (z), density (rho)] |
    % |                         Using flux probability                     |
    % |                  with 1 dimension for depth and density            |
    % |                         in stochastic motion                       |
    % |                      using Crank-Nicolson method                   | 
    % |                                                                    |
    % | Author: Maximilien Bricout                              01/08/2022 | 
    % +--------------------------------------------------------------------+
    
    
% You have to define: 
    %  -velocity function for x, y, z. -> at the end
    %  -Derivate function for density rho -> at the end
    %  -diffusion tensor for x, y, z, rho -> at the end
    %  -the time of the simulation -> in custom parameters



% Details: 
    % Solving P(x,y,rho,z,t)
    % rho [kg/m^3], z[m], x[m], y[m], t[day]
    % Rho an z are acting on 1stD: Row, 
    % x on vertical 2ndD: column 
    % y on horizontale 3rdD: page
    % t on time 4thD 


% Gitlab links: 
% email: maximilien.bricout@etu.utc.fr 

%% Start
    clear, clc, tic

     export_video=0;                       % 1=yes, 0=No
     Save_data=0;                          % 1=yes, 0=No  

%% custom parameters
     
    Simulation_time= 1;                  % Your choice
    
% Define those function at the end:                                     
    %     Velocity_x 
    %     Velocity_y
    %     Velocity_z
    %     Density_derivate
    %     Diffusion tensor sigma_x
    %     Diffusion tensor sigma_y
    %     Diffusion tensor sigma_z
    %     Diffusion tensor sigma_r

%% Grid specific: z,x,y,t,rho 

%Space discretization
    N=32;           % default discretization step

    % Z   % spatial spacing %apatial grid sections
        L=80;                           % max depth; if L depends on Particle parameter create condition here
        Nz=N;                           % discretization step in depth
        z=-[0.5:Nz]'/Nz*L;              % Set up of all step                                               
        dz=z(1)-z(2);                   % difference btw each step
    
    % X    % spatial spacing  %apatial grid sections
        Lx=80;                         % max vertical; if L depends on Particle parameter create condition here
        Nx=N;                         % discretization step in vertical
        x=[0.5:Nx]'/Nx*Lx;              % Set up of all step 
        dx=abs(x(1)-x(2));              % difference btw each step
    
    % Y   % spatial spacing  %apatial grid sections 
        Ly=80;                         % max horizontal; if L depends on Particle parameter create condition here
        Ny=N;                         % discretization step in horizontal
        y=[0.5:Ny]'/Ny*Ly;              % Set up of all step 
        dy=abs(y(1)-y(2));              % difference btw each step
    
    % Time discretization & precision
       tsnap=32;                        % Result keep
       dt=1/1024;                       % time discretization     

    % density discretization % Density spacing  
        dr=N/32 ;                          % particle parmeter discretization: depend on the kind of parameter and range value

                     


%% Initial conditions
    P=zeros(Nz,Nx,Ny,1);                                            
    P(Nz/2,Nx/2,Ny/2)=1;
    t=0;

%% Parameters: global result variable initialization

    Ps(:,:,:,1)=[P];                                      
    ts=[t];                                       

%% Solving FPE
    
    while t<Simulation_time
   
      
    % At t    %Flux parameters                                                                        
              u=velx(t,x,Nx,y,Ny,z,Nz); 
              v=vely(t,x,Nx,y,Ny,z,Nz);
              w= velz(t,x,Nx,y,Ny,z,Nz);
              rhop= dens(t,x,Nx,y,Ny,z,Nz);
              sigz=sigmaz(t,x,Nx,y,Ny,z,Nz); 
              sigr=sigmar(t,x,Nx,y,Ny,z,Nz); 
              sigx=sigmax(t,x,Nx,y,Ny,z,Nz); 
              sigy=sigmay(t,x,Nx,y,Ny,z,Nz); 

              %Flux calculus
              [Fw,Fsw,Fr,Fsr,Fu,Fsu,Fv,Fsv]=flux(P,w,sigz,rhop,sigr,u,sigx,v,sigy,Nz,Nx,Ny);   
                                                                
              % First part of FPE resolution 
              dP = dt*( (Fw(2:end,1:end,1:end)-Fw(1:end-1,1:end,1:end))/(dz)   +(Fsw(2:end,1:end,1:end)+Fsw(1:end-1,1:end,1:end))/(dz^2)    ...
                   +(Fr(2:end,1:end,1:end)-Fr(1:end-1,1:end,1:end))/(dr)   +(Fsr(2:end,1:end,1:end)+Fsr(1:end-1,1:end,1:end))/(dr^2)            ...
                   +(Fu(1:end,2:end,1:end)-Fu(1:end,1:end-1,1:end))/(dr)   +(Fsu(1:end,2:end,1:end)+Fsu(1:end,1:end-1,1:end))/(dr^2)                ...
                   +(Fv(1:end,1:end,2:end)-Fv(1:end,1:end,1:end-1))/(dr)   +(Fsv(1:end,1:end,2:end)+Fsv(1:end,1:end,1:end-1))/(dr^2) );                                
            
    
    % At t+dt
              t=t+dt;
              %Flux parameters
              u=velx(t,x,Nx,y,Ny,z,Nz); 
              v=vely(t,x,Nx,y,Ny,z,Nz);
              w= velz(t,x,Nx,y,Ny,z,Nz);
              rhop= dens(t,x,Nx,y,Ny,z,Nz);
              sigz=sigmaz(t,x,Nx,y,Ny,z,Nz); 
              sigr=sigmar(t,x,Nx,y,Ny,z,Nz); 
              sigx=sigmax(t,x,Nx,y,Ny,z,Nz); 
              sigy=sigmay(t,x,Nx,y,Ny,z,Nz);  
              %Flux calculus
              [Fw,Fsw,Fr,Fsr,Fu,Fsu,Fv,Fsv]=flux(P,w,sigz,rhop,sigr,u,sigx,v,sigy,Nz,Nx,Ny);                                                                     
              % First part of FPE resolution 
              dP1 = dt*( (Fw(2:end,1:end,1:end)-Fw(1:end-1,1:end,1:end))/(dz)   +(Fsw(2:end,1:end,1:end)+Fsw(1:end-1,1:end,1:end))/(dz^2)    ...
                   +(Fr(2:end,1:end,1:end)-Fr(1:end-1,1:end,1:end))/(dr)   +(Fsr(2:end,1:end,1:end)+Fsr(1:end-1,1:end,1:end))/(dr^2)            ...
                   +(Fu(1:end,2:end,1:end)-Fu(1:end,1:end-1,1:end))/(dr)   +(Fsu(1:end,2:end,1:end)+Fsu(1:end,1:end-1,1:end))/(dr^2)           ...
                   +(Fv(1:end,1:end,2:end)-Fv(1:end,1:end,1:end-1))/(dr)   +(Fsv(1:end,1:end,2:end)+Fsv(1:end,1:end,1:end-1))/(dr^2) ); 
    
   % Solving FPE 
              P=(P*(1-sigz*dt/dz^2-sigr*dt/dr^2-sigx*dt/dx^2-sigy*dt/dy^2)+0.5*(dP+dP1))/(1+sigz*dt/dz^2+sigr*dt/dr^2+sigx*dt/dx^2+sigy*dt/dy^2);                                  
    
    
   % live plot FPE: depth=f(probability)          
              if rem(t,tsnap*dt)==0                            
                ts=[ts,t];                                              
                Ps(:,:,:,end+1)=[P];  
                plot(P(:,Nx/2,Ny/2),z);
                title(['Data = \it{P} \rm(Z, t) @ t = ' num2str(t)]) 
                drawnow();
              end
    end


%% Plot 5D, 2D & video export
   % Set up for video exportation
            if Save_data==1; save('Datasaved/Datapar.mat','x','y','z','ts'); end
   % Set up for visualization
            x=x(1:end,1)';
            y=y(1:end,1)';
            z=z(1:end,1)';
            t=ts(1,1:end);
            [X, Y, Z] = meshgrid(x, z, y);
    
    % form the data matrix - it is the fifth dimension
            Data=Ps(1:end,1:end,1:end,1:end);
            Data2Dxy=squeeze(sum(Data,1));
            Data2Dzy=squeeze(sum(Data,2));
            Data2Dzx=squeeze(sum(Data,3));
            Data2Dxy(Data2Dxy<=0.00001)=NaN;    %enable clear comprehension of graphics
            Data(Data<=0.00001)=NaN;            %enable clear comprehension of graphics
            
    % organize the visualization
            figure(1) 
            hScatter = scatter3(X(:), Z(:), Y(:), 'filled');
            colormap cool
            grid3 on
            set(gca, 'FontName', 'Times New Roman', 'FontSize', 14)
            xlabel('Horizontal (km)')
            ylabel('Vertical (km)')
            zlabel('Depth (m)')
            c = colorbar;
            text(48,-1000,-32,'Probability')

    % saving video 
            if export_video==1
                    if isunix % for linux
                    pathVideoAVI = '~/someVideo2.avi'; % filename, used later to generate mp4
                    elseif ispc % fow windows
                    pathVideoAVI = 'Repport/someVideo2.avi'; % filename, used later to generate mp4
                    end
                    writerObj = VideoWriter(pathVideoAVI,'Uncompressed AVI');
                    writerObj.FrameRate=10;
                    open(writerObj);
                    if isunix % for linux
                    pathVideoAVI = '~/someVideo2.avi'; % filename, used later to generate mp4
                    elseif ispc % fow windows
                    pathVideoAVI = 'Repport/someVideo3.avi'; % filename, used later to generate mp4
                    end
                    writerObj2 = VideoWriter(pathVideoAVI,'Uncompressed AVI');
                    writerObj2.FrameRate=10;
                    open(writerObj2);
            end


            
    % cycle through the fourth dimension independent variable
            for k = 1:size(t,2) 
                
                figure(1)
                C = reshape(Data(:, :, :,k), length(x)*length(y)*length(z), 1);         % visualize the fifth dimension via the marker color
                set(hScatter, 'CData', C, 'SizeData', 10)                               % update the plot
                title([' \rm\it Figure 8: Probability distribution represented for a PP particle of 0.1mm @ t = ' num2str(t(k)),newline])
                drawnow
                pause(0.1)


                if export_video==1
                                if k~=1
                                frame = getframe(gcf);
                                writeVideo(writerObj,frame);
                                end
                end
  
            
    % 2Dplot : x/y
                figure(2)
                    h=pcolor(x,y,Data2Dxy(:,:,k));
                    set(h, 'EdgeColor', 'none');
                    set(gca,'color',[0.95 0.95 0.95])
                    title(['\rm\it Figure 7: Probability distribution represented for a PP particle of 0.1mm @ t = ' num2str(t(k)),newline])
                    xlabel('Horizontal (km)')
                    ylabel('Vertical (km)')
                    colormap cool
                    c = colorbar;
                    text(795,-10,'Probability')
                    drawnow
    % 2nd video
                if export_video==1
                if k~=1
                frame = getframe(gcf);
                writeVideo(writerObj2,frame);
                end
                end
            
            end
    % video converter
                if export_video==1
                % Close the movie file
                close(writerObj); 
                % convert AVI to MP4
                pathVideoMP4 = regexprep(pathVideoAVI,'\.avi','.mp4'); % generate mp4 filename
                if isunix % for linux
                    [~,~] = system(sprintf('ffmpeg -i %s -y -an -c:v libx264 -crf 0 -preset slow %s',pathVideoAVI,pathVideoMP4)); % for this to work, you should have installed ffmpeg and have it available on PATH
                elseif ispc % for windows
                    [~,~] = system(sprintf('ffmpeg.exe -i %s -y -an -c:v libx264 -crf 0 -preset slow %s',pathVideoAVI,pathVideoMP4)); % for this to work, you should have installed ffmpeg and have it available on PATH
                end
                % Close the movie file
                close(writerObj2); 
                % convert AVI to MP4
                pathVideoMP4 = regexprep(pathVideoAVI,'\.avi','.mp4'); % generate mp4 filename
                if isunix % for linux
                    [~,~] = system(sprintf('ffmpeg -i %s -y -an -c:v libx264 -crf 0 -preset slow %s',pathVideoAVI,pathVideoMP4)); % for this to work, you should have installed ffmpeg and have it available on PATH
                elseif ispc % for windows
                    [~,~] = system(sprintf('ffmpeg.exe -i %s -y -an -c:v libx264 -crf 0 -preset slow %s',pathVideoAVI,pathVideoMP4)); % for this to work, you should have installed ffmpeg and have it available on PATH
                end
                end 

toc                                                                       % End timer 
%% Method function

    % Flow  Function method 
            function [Fw,Fsw,Fr,Fsr,Fu,Fsu,Fv,Fsv]=...
                                        flux(P,w,sigwk,rhop,sigr,u,sigx,v,sigy,Nz,Nx,Ny)   

       % Flux
            pa=P(1:end-1,1:end,1:end)+P(2:end,1:end,1:end);
            pd=P(1:end-1,1:end,1:end)-P(2:end,1:end,1:end);
            pa2=P(1:end,1:end-1,1:end)+P(1:end,2:end,1:end);
            pd2=P(1:end,1:end-1,1:end)-P(1:end,2:end,1:end);
            pa3=P(1:end,1:end,1:end-1)+P(1:end,1:end,2:end);
            pd3=P(1:end,1:end,1:end-1)-P(1:end,1:end,2:end);

      % Z velocity + diffusion
            w=w(end-1,:,:);
            Fw=[0.5*w.*pa-0.5*abs(w).*pd];
            Fw= [zeros(1,Nx,Ny); Fw  ; zeros(1,Nx,Ny) ];
            Fsw=[sigwk.*pd];
            Fsw= [zeros(1,Nx,Ny); Fsw  ; zeros(1,Nx,Ny) ];

      % Rho velocity + diffusion
            Fr=[0.5*rhop.*pa-0.5*abs(rhop).*pd];
            Fr= [zeros(1,Nx,Ny); Fr  ; zeros(1,Nx,Ny) ];
            Fsr=[sigr.*pd];
            Fsr= [zeros(1,Nx,Ny); Fsr  ; zeros(1,Nx,Ny) ];

      % X velocity + diffusion  
            u=u(:,end-1,:);
            Fu=[1*(0.5*u.*pa2-0.5*abs(u).*pd2)];
            Fu= [zeros(Nz,1,Ny) Fu zeros(Nz,1,Ny) ];
            Fsu=[sigx.*pd2];
            Fsu= [zeros(Nz,1,Ny) Fsu zeros(Nz,1,Ny)  ];

      %Y velocity + diffusion
            v=v(:,:,end-1);
            Fv=[1*(0.5*v.*pa3-0.5*abs(v).*pd3)];
            RR=zeros(Nz,Nx,1);
            Fv= cat(3,RR,Fv,RR);
            Fsv=[sigy.*pd3];
            RR=zeros(Nz,Nx,1);
            Fsv= cat(3,RR,Fsv,RR);
            end 


%% Function to be defined

    % Velocity U
            function U=velx(t,x,Nx,y,Ny,z,Nz) 
            x=x';
            x=repmat(x,Nz,1);
            x=repmat(x,1,1,Ny);
            z=repmat(z,1,Nz);
            z=repmat(z,1,1,Ny);
            for i=1:Ny; y1(1,1,i)=y(i); end
            for j=1:Ny
            for i=1:Nx; y2(:,i,j)=y1(1,1,j);end
            for i=1:Nz; y3(i,:,j)=y2(1,:,j);end
            end
            y=y3;

            U=2*cos(x*t);                                   % Define velocity function
            end
    
    % Velocity V
            function V=vely(t,x,Nx,y,Ny,z,Nz)
            x=x';
            x=repmat(x,Nz,1);
            x=repmat(x,1,1,Ny);
            z=repmat(z,1,Nz);
            z=repmat(z,1,1,Ny);
            for i=1:Ny; y1(1,1,i)=y(i); end
            for j=1:Ny
            for i=1:Nx; y2(:,i,j)=y1(1,1,j);end
            for i=1:Nz; y3(i,:,j)=y2(1,:,j);end
            end
            y=y3;
            V=0.2*abs(y*t);                                    % Define velocity function
            end
    
    % Velocity w
            function W=velz(t,x,Nx,y,Ny,z,Nz)
            x=x';
            x=repmat(x,Nz,1);
            x=repmat(x,1,1,Ny);
            z=repmat(z,1,Nz);
            z=repmat(z,1,1,Ny);
            for i=1:Ny; y1(1,1,i)=y(i); end
            for j=1:Ny
            for i=1:Nx; y2(:,i,j)=y1(1,1,j);end
            for i=1:Nz; y3(i,:,j)=y2(1,:,j);end
            end
            y=y3; 

            W=12*sin(z*t);                                    % Define velocity function

            end
    
    % Rho derivate 
            function rhop=dens(t,x,Nx,y,Ny,z,Nz) 
                rhop=4*t;                                            % Define derivate function
            end

    % X Sigma
            function sigx=sigmax(t,x,Nx,y,Ny,z,Nz)
        sigx=0;                                              % Define diffusion tensor function
    end
    
    % Y Sigma
            function sigy=sigmay(t,x,Nx,y,Ny,z,Nz)
        sigy=0;                                               % Define diffusion tensor function
    end
   
    % Z Sigma
            function sigz=sigmaz(t,x,Nx,y,Ny,z,Nz)
        sigz=0;                                               % Define diffusion tensor function
    end
    
    % Rho Sigma
            function sigr=sigmar(t,x,Nx,y,Ny,z,Nz)
        sigr=0;                                               % Define diffusion tensor function
    end
              
            
    
    
%Ps: suppress warning message
    %#ok<*AGROW>
    %#ok<*NBRAK> 
    %#ok<*SAGROW> 
    %#ok<*INUSD> 
    %#ok<*NASGU> 




